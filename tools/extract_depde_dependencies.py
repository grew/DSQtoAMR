# -*- coding: utf-8 -*-
import os,re,codecs
input_path = '/Users/perrier/recherche/deep-sequoia/trunk/sequoia_deep.conll'
output_path = '/Users/perrier/recherche/GREW_resources/AMR/experiment/depde_dependencies.lp'
   
finput=codecs.open(input_path, mode='r', encoding='utf-8')
foutput=codecs.open(output_path, mode='w', encoding='utf-8')
corpus = finput.readlines()
finput.close()
lcorpus = len(corpus)
n=1
lexicon =[]
#while n < 200:
while n < lcorpus:
    l= corpus[n]
    #print l
    sent=[]
    while l <> '\n':
        lentry = l.split('\t')
        #print lentry
        entry = { "lemma":lentry[2], "gov":lentry[6], "funct":lentry[7]}
        #print entry
        sent.append(entry)
        n +=1
        l = corpus[n]
    lsent=len(sent)
    for i in range(lsent):
        token = sent[i]
        if token["funct"] == u"dep.de":
            gov_list= token["gov"].split(u"|")
            for gov in gov_list:
                govn=int(token["gov"])-1
                gov= sent[govn]["lemma"]
                lexicon.append(gov  + u"#" +token["lemma"])
    n +=2
for entry in lexicon:
    foutput.write(entry + u"\n")
    
foutput.close()
    
