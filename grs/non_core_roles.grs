% ===============================================================================================
% Transformation of DEP.DE dependencies into AMR relations.
package non_core_dep_de{

% The DEP.DE dependency is transformed into a CAUSE relation.
  rule cause_inf {
    pattern{
      N[cat=N,pos=NC,lemma=lex.noun];COMPL[cat=V,m=inf];
      depde_rel: N -[dep.de]-> COMPL}
    commands{del_edge depde_rel;add_edge N -[cause]-> COMPL}}
#BEGIN lex
noun
%---------
bonheur
#END

  rule consist1(lex from "lexicons/word_class/noun-pred_consist.lp"){
    pattern{
      N[cat=N,pos=NC,lemma=lex.noun];COMPL[cat=N];
      depde_rel: N -[dep.de]-> COMPL}
    commands{
      del_edge depde_rel;
      add_edge N -[consist]-> COMPL}}

  rule consist2(lex from "lexicons/word_class/noun-pred_consistof.lp"){
    pattern{
      N[cat=N,pos=NC];COMPL[cat=N,lemma=lex.noun];
      depde_rel: N -[dep.de]-> COMPL}
    commands{
      del_edge depde_rel;
      add_edge N -[consist]-> COMPL}}

  rule consistof(lex from "lexicons/word_class/noun-pred_consistof.lp"){
    pattern{
      N[cat=N,pos=NC,lemma=lex.noun];COMPL[cat=N];
      depde_rel: N -[dep.de]-> COMPL}
   % without{COMPL -[det]-> *}
    commands{
      del_edge depde_rel;
      add_edge N -[consist-of]-> COMPL}}

  rule name(lex from "lexicons/word_class/noun-pred_name.lp"){
    pattern{
      N[cat=N,pos=NC,lemma=lex.noun];COMPL[cat=N,pos=NPP];
      depde_rel: N -[dep.de]-> COMPL}
    commands{
      del_edge depde_rel;
      add_edge N -[name]-> COMPL}}

% If the DEP.DE complement is a proper noun, it is considered as a possessor.
  rule poss1{
    pattern{
      N[cat=N];COMPL[cat=N,pos=NPP];
      depde_rel: N -[dep.de]-> COMPL}
    commands{del_edge depde_rel; add_edge N -[poss]-> COMPL}}

% If the DEP.DE complement is a common noun, it is considered as a possessor if it has a determiner and its governor belongs to a lexicon.
  rule poss2(lex from "lexicons/word_class/noun-pred_poss.lp"){
    pattern{
      N[cat=N,lemma=lex.noun];COMPL[pos=NC];
      COMPL -[det]-> *;
      depde_rel: N -[dep.de]-> COMPL}
    commands{del_edge depde_rel; add_edge N -[poss]-> COMPL}}

% If the DEP.DE complement is a common noun, it is considered as a possessor if it is in the plural and its governor belongs to a lexicon.
  rule poss3(lex from "lexicons/word_class/noun-pred_poss.lp"){
    pattern{
      N[cat=N,lemma=lex.noun];COMPL[cat=N,pos=NC,n=p];
      depde_rel: N -[dep.de]-> COMPL}
    commands{del_edge depde_rel; add_edge N -[poss]-> COMPL}}

% If the DEP.DE complement is a common noun, it is considered as a possessor if it is a clitic and its governor belongs to a lexicon.
  rule poss4(lex from "lexicons/word_class/noun-pred_poss.lp"){
    pattern{
      N[cat=N,lemma=lex.noun];COMPL[cat=CL];
      depde_rel: N -[dep.de]-> COMPL}
    commands{del_edge depde_rel; add_edge N -[poss]-> COMPL}}

  rule purpose(lex from "lexicons/word_class/noun-pred_purpose.lp"){
    pattern{
      N[cat=N,pos=NC,lemma=lex.noun];COMPL[cat=N,pos=NC];
      depde_rel: N -[dep.de]-> COMPL}
    without{COMPL -[det]-> *}
    commands{
      del_edge depde_rel;
      add_edge N -[purpose]-> COMPL}}

  rule topic(lex from "lexicons/word_class/noun-pred_topic.lp"){
    pattern{
      N[cat=N,pos=NC,lemma=lex.noun];COMPL[cat=N,n=s];
      depde_rel: N -[dep.de]-> COMPL}
    commands{
      del_edge depde_rel;
      add_edge N -[topic]-> COMPL}}


}
% ===============================================================================================
% Modifier prepositional phrases
package prep_modif{

  rule cause{
    pattern{
      MOD[cat=P,type=cause]; mod_rel: GOV -[mod]-> MOD;
      obj_rel: MOD -[obj.p]-> OBJ}
    commands{
      del_edge mod_rel; del_edge obj_rel;
      shift_out MOD ==> OBJ;
      MOD.void=y; add_edge GOV -[cause]-> OBJ}}

  rule destination{
    pattern{PREP[cat=P,type=destination];dep_rel: GOV -[dep|mod]-> PREP}
    commands{del_edge dep_rel;add_edge GOV -[destination]-> PREP}}

  rule duration{
    pattern{PREP[cat=P,type=duration];dep_rel: GOV -[dep|mod]-> PREP}
    commands{del_edge dep_rel;add_edge GOV -[duration]-> PREP}}

% The expression ("de" time noun "à" time noun) plays a DURATION role.
  rule duration_de_a{
    pattern{
      dep_rel: GOV -[dep|mod]-> DE; DE[cat=P,lemma="de"];
      objp_rel1: DE -[obj.p]-> T1;
      A[cat=P, lemma="à"]; arg_rel: DE -[arg]-> A;
      objp_rel2: A -[obj.p]-> T2}
    without{T1[cat=N,!type,s<>card]}
    without{T1[cat=N,type<>time]}
    without{T1[cat=PRO,s<>card]}
    without{T2[cat=N,!type,s<>card]}
    without{T2[cat=N,type<>time]}
    without{T2[cat=PRO,s<>card]}
    commands{
       del_edge dep_rel; del_edge arg_rel;
       del_edge objp_rel1; del_edge objp_rel2;
       add_edge GOV -[duration]-> DE; A.void=y;
       DE.concept="période";
       add_edge DE -[op1]-> T1; add_edge DE -[op2]-> T2}}

  rule location{
    pattern{PREP[cat=P, type=location];dep_rel: GOV -[dep|mod]-> PREP}
    without{PREP[lemma="en"]; OBJP[s=card]; PREP -[obj.p]-> OBJP}
    without{PREP[lemma="en"]; OBJP[cat=V,dm=pstpart]; PREP -[obj.p]-> OBJP}
    without{OBJP[type=abs]; PREP -[obj.p]-> OBJP}
    commands{del_edge dep_rel;add_edge GOV -[location]-> PREP}}

% There is a LOCATION relation from the modified word to the modifier semantic head realized withthe prepostion "à" introducing a proper noun or a location common noun.
  rule location_a_noun{
    pattern{
      PREP[cat=P,lemma="à"];OBJP[cat=N];
      objp_rel: PREP -[obj.p]-> OBJP;
      dep_rel: GOV -[dep|mod]-> PREP}
    without{ OBJP[pos=NC,!type]}
    without{OBJP[pos=NC,type <> location]}
    commands{
      del_edge dep_rel;del_edge objp_rel;
      add_edge GOV -[location]-> OBJP;
      shift_out PREP ==> OBJP;
      PREP.void=y}}

% The expression ("de" location noun "à" location noun) plays a LOCATION role.
  rule location_de_a{
    pattern{
      dep_rel: GOV -[dep|mod]-> DE; DE[cat=P,lemma="de"];
      objp_rel1: DE -[obj.p]-> T1;
      A[cat=P, lemma="à"]; arg_rel: DE -[arg]-> A;
      objp_rel2: A -[obj.p]-> T2}
    without{T1[cat=N,!type]}
    without{T1[cat=N,type<>location]}
    without{T2[cat=N,!type]}
    without{T2[cat=N,type<>location]}
    commands{
       del_edge dep_rel; del_edge arg_rel;
       del_edge objp_rel1; del_edge objp_rel2;
       add_edge GOV -[location]-> DE; A.void=y;
       DE.concept="lieu";
       add_edge DE -[op1]-> T1; add_edge DE -[op2]-> T2}}

% There is a PURPOSE relation from the modified word to the modifier semantic head, which is an infinitive introduced with a preposition given by the lexicon below.
  rule purpose_inf{
    pattern{
      PREP[cat=P,type=purpose];
      dep_rel: GOV -[dep|mod]-> PREP;
      OBJP[cat=V,m=inf];
      objp_rel : PREP -[obj.p]-> OBJP}
    commands{
      del_edge dep_rel; del_edge objp_rel;
      PREP.void=y;
      shift_out PREP ==> OBJP;
      add_edge GOV -[purpose]-> OBJP}}

  rule time{
    pattern{PREP[cat=P, type=time];dep_rel: GOV -[dep|mod]-> PREP}
    commands{del_edge dep_rel; add_edge GOV -[time]-> PREP}}

% There is a TIME relation from the modified word to the modifier semantic head realized with the preposition "à" introducing a time common noun.
  rule time_a_noun{
    pattern{
      PREP[cat=P,lemma="à"];OBJP[cat=N,type=time];
      objp_rel: PREP -[obj.p]-> OBJP;
      dep_rel: GOV -[dep|mod]-> PREP}
    commands{
      del_edge dep_rel;del_edge objp_rel;
      shift_out PREP ==> OBJP;
      add_edge GOV -[time]-> OBJP;
      PREP.void=y}}

% The preposition "en" governs a noun of type time or a cardinal, and then, there is a TIME relation from the modified word to the modifier.
  rule time_en{
    pattern{
      PREP[cat=P, lemma="en"];
      dep_rel: GOV -[dep|mod]-> PREP;
      objp_rel: PREP -[obj.p]-> OBJP}
    without{OBJP[cat=N,!type]}
    without{OBJP[cat=N,type <>time]}
    without{OBJP[cat=PRO,s<>card]}
    commands{
      del_edge dep_rel;del_edge objp_rel;
      shift_out PREP ==> OBJP;
      PREP.void=y;add_edge GOV -[time]-> OBJP}}

  rule prep_topic{
    pattern{PREP[cat=P,type=topic];dep_rel: GOV -[dep|mod]-> PREP}
    commands{del_edge dep_rel; add_edge GOV -[topic]-> PREP}}

% The preposition "en" has a number as its dependent, which represents a year. As a consequence, there is a YEAR relation from the modified word to the modifier semantic head.
  rule year_en_card{
    pattern{
      EN[cat=P, lemma="en"];
      dep_rel: GOV -[dep|mod]-> EN;
      YEAR[cat=N,s=card];
      objp_rel: EN -[obj.p]-> YEAR}
    commands{
      del_edge dep_rel; del_edge objp_rel;
      shift_out EN ==> YEAR;
      EN.void=y;add_edge GOV -[year]-> YEAR}}

}
% ===============================================================================================
% Adverbial clauses.
package advcl_modif{

  rule cause{
    pattern{
      MOD[pos=CS,type=cause]; mod_rel: GOV -[mod]-> MOD;
      obj_rel: MOD -[obj.cpl]-> OBJ}
    commands{
      del_edge mod_rel; del_edge obj_rel;
      MOD.void=y; add_edge GOV -[cause]-> OBJ}}

  rule condition{
    pattern{
      MOD[pos=CS,type=condition]; mod_rel: GOV -[mod]-> MOD;
      obj_rel: MOD -[obj.cpl]-> OBJ}
    commands{
      del_edge mod_rel; del_edge obj_rel;
      MOD.void=y; add_edge GOV -[condition]-> OBJ}}
}

% ===============================================================================================
% Adverbs behaving as modifiers.
package adv_modif{

  rule degree{
    pattern{ADV[cat=ADV,type=degree];mod_rel: GOV -[mod]-> ADV}
    commands{del_edge mod_rel; add_edge GOV -[degree]-> ADV}}

% The modifier is a superlative adverb.
  rule degree_super{
    pattern{ modsuper_rel: ADJ -[mod.super]-> ADV}
    without{ADV -[ARG1]-> ADJ}
    commands{
      del_edge modsuper_rel;ADV.super=y;
      add_edge ADV -[ARG1]-> ADJ}}

  rule frequency{
    pattern{ADV[cat=ADV,type=frequency];dep_rel: GOV -[dep|mod]-> ADV}
    commands{del_edge dep_rel;add_edge GOV -[frequency]-> ADV}}

  rule manner{
    pattern{MOD[cat=ADV, type=manner];dep_rel: GOV -[dep|mod]-> MOD}
    commands{del_edge dep_rel;add_edge GOV -[manner]-> MOD}}

  rule time{
    pattern{PREP[cat=ADV,type=time];dep_rel: GOV -[dep|mod]-> PREP}
    commands{del_edge dep_rel; add_edge GOV -[time]-> PREP}}

}

% ===============================================================================================
% Adjectives behaving as modifiers
package adj_modif{

% The subject of the adjective is linked with it by a DOMAIN relation.
  rule adj_suj-domain{
    pattern{ADJ[cat=A|ADV|N|P]; suj_rel: ADJ -[suj]-> SUJ}
    without{ SUJ -[mod.comp]-> ADJ}
    commands{
      del_edge suj_rel;
      add_edge ADJ -[domain]-> SUJ}}

% There is a QUANT relation from the modified word to the modifier, which is cardinal adjective.
  rule adj_card1(lex from "lexicons/word_class/month.lp"){
    pattern{
      CARD[cat=A,s=card]; N[cat=N|PRO];
      mod_rel: N -[mod]-> CARD; CARD << N}
    without{N[lemma=lex.month]}
    commands{del_edge mod_rel; add_edge N -[quant]-> CARD}}

% There is a AMR-MOD relation from the modified word to the modifier, which is cardinal adjective.
  rule adj_card2{
    pattern{
      CARD[cat=A,s=card];
      mod_rel: N -[mod]-> CARD; N << CARD}
    commands{del_edge mod_rel; add_edge N -[amr-mod]-> CARD}}

% There is a DAY relation from the modified word to the modifier, which is cardinal adjective.
  rule adj_day(lex from "lexicons/word_class/month.lp"){
    pattern{
      DAY[cat=A,s=card]; N[cat=N|PRO,lemma=lex.month];
      mod_rel: N -[mod]-> DAY; DAY << N}
    commands{
      del_edge mod_rel; N.concept=date;
      add_edge N -[day]-> DAY}}

% There is an ORD relation from the modified word to the modifier, which is an ordinal adjective.
  rule adj_ord{
    pattern{
      MOD[cat=A,s=ord];
      arg0_rel: MOD -[ARG0]->GOV;
      mod_rel: GOV -[mod]-> MOD}
    commands{
      del_edge mod_rel; del_edge arg0_rel;
      add_edge GOV -[ord]-> MOD}}

}

% ===============================================================================================
% Nouns behaving as direct modifiers
package noun_modif{

% The modifier is the name of the modified entity, which is proper noun.
  rule noun_name1{
    pattern{ N[pos=NPP];MOD[pos=NPP]; mod_rel: N -[mod]-> MOD}
    commands{del_edge mod_rel; add_edge N -[name]-> MOD}}

% The modifier is the name of the modified entity, which is a common noun from the lexicon below.
  rule noun_name2 {
    pattern{ N[pos=NC,lemma=lex.lemma];MOD[pos=NPP]; mod_rel: N -[mod]-> MOD}
    commands{del_edge mod_rel; add_edge N -[name]-> MOD}}
#BEGIN lex
lemma
%------------
cabinet
monsieur
#END

% The modifier is a year noun
  rule noun_year(lex from "lexicons/word_class/month.lp"){
    pattern{
      MOD[cat=N,s=card];N[cat=N,lemma=lex.month];
      mod_rel: N -[mod]-> MOD}
    commands{
      N.concept=date;
      del_edge mod_rel; add_edge N -[year]-> MOD}}


% The modifier is a time nouns
  rule noun_time{
    pattern{ MOD[cat=N,type=time];mod_rel: W -[mod]-> MOD}
    commands{del_edge mod_rel; add_edge W -[time]-> MOD}}

% Apposition of a noun without determiner.
  rule appos{
    pattern{
      N[cat=N];
      APPOS[cat=N];
      modapp_rel: N -[mod.app]-> APPOS}
    without{APPOS -> N}
    commands{del_edge modapp_rel; add_edge N -[amr-mod]-> APPOS}}

% The pronoun "ce" has a function of apposition.
  rule ce_appos{
    pattern{
      CE[cat=PRO,lemma="ce"];
      GOV[cat=N|PRO|V];
      mod_rel: GOV -[mod|mod.app]-> CE}
    commands{
      del_edge mod_rel; CE.void=y;
      shift CE ==> GOV}}

% Vocative dependencies
  rule noun_voc{
    pattern{N[cat=N|PRO];modvoc_rel: V -[mod.voc]-> N}
    commands{del_edge modvoc_rel; add_edge V -[vocatif]-> N}}

}

% ===============================================================================================
% Transformation of negative indications into the feature "polarity = -"
package negation{

% Precondition: A predicate is modified by "ne" and another negative adverb from the lexicon below.
% Action : "ne" is deleted, the other adverb is transformed into the positive dual adverb with a TIME dependency to the predicate and the predicate is marked with "polarity= -".
  rule neg_adv {
    pattern{
      NE[cat=ADV,lemma="ne"]; PRED[cat=A|V];
      mod_rel1: PRED -[mod]-> NE;
      ADV[cat=ADV,lemma=lex.neg]; mod_rel2: PRED -> ADV}
      commands{
        del_edge mod_rel1;  del_edge mod_rel2; add_edge PRED -[time]-> ADV;
	ADV.concept=lex.concept;NE.void=y;PRED.polarity= "-"}}
#BEGIN lex
neg	concept
%-------------------------
guère	beaucoup
jamais	parfois
plus	encore
#END

% Precondition: A predicate is modified by "ne" and "pas".
% Action : "ne" and "pas are deleted and the predicate is marked with "polarity= -".
  rule neg_pas{
    pattern{
      NE[cat=ADV,lemma="ne"]; PRED[cat=A|V];
      mod_rel1: PRED -[mod]-> NE;
      PAS[cat=ADV,lemma="pas"];
      mod_rel2: PRED -[mod]-> PAS}
      commands{
        del_edge mod_rel1;del_edge mod_rel2;
	NE.void=y;PAS.void=y;PRED.polarity= "-"}}

% Precondition: A predicate is modified by "ne" and one of its arguments by "que".
% Action : "ne" is deleted and "que" is associated with the concept "seulement"; its dependency to the argument is transformed into a AMR-MOD dependency.
  rule neg_que{
    pattern{
      NE[cat=ADV,lemma="ne"]; PRED[cat=A|V];
      mod_rel1: PRED -[mod]-> NE;
      PRED -> ARG;
      QUE[cat=ADV,lemma="que"];
      mod_rel2: ARG -[mod]-> QUE}
      commands{
        del_edge mod_rel1;del_edge mod_rel2;
	NE.void=y; QUE.concept=seulement; add_edge ARG -[amr-mod]-> QUE}}

% Precondition: A predicate is modified by "ne" and one of its arguments has a negative determiner.
% Action : "ne" is deleted and the argument is marked with "polarity = - ".
  rule neg_det{
    pattern{
      NE[cat=ADV,lemma="ne"]; PRED[cat=A|V];
      mod_rel: PRED -[mod]-> NE;
      ARG[cat=A|N|PRO|V];
      PRED -> ARG;
      DET[cat=D, lemma ="aucun"|"nul"];
      ARG -[det]-> DET}
      commands{
      del_edge mod_rel; NE.void=y;ARG.polarity= "-"}}

% Precondition: A predicate is modified by "ne" and one of its arguments is a negative pronoun.
% Action : "ne" is deleted and the argument is marked with "polarity = - ".
  rule neg_pro {
    pattern{
      NE[cat=ADV,lemma="ne"]; V[cat=V];
      mod_rel: V -[mod]-> NE;
      PRO[cat=PRO,lemma=lex.lemma];
      V -> PRO}
      commands{del_edge mod_rel; NE.void=y;PRO.concept=lex.concept; PRO.polarity= "-"}}
#BEGIN lex
lemma	concept
%--------------------
aucun	quelqu'un
nul	quelqu'un
personne	quelqu'un
rien	quelque_chose
#END

% Precondition: an infinitive is modified by "ne_pas".
% Action: the infinitive is marked with the feature "polarity = - " and "ne_pas" is deleted.
  rule ne_pas{
    pattern{NEG[cat=ADV,lemma="ne_pas"]; V[cat=V,m=inf]; mod_rel: V -[mod]-> NEG}
    commands{NEG.void=y; V.polarity="-"; del_edge mod_rel}}

% Precondition: a word is modified by "non".
% Action: the word is marked with the feature "polarity = - " and "non" is deleted.
  rule non{
    pattern{
      NON[cat=ADV,lemma="non"];
      mod_rel: GOV -[mod]-> NON}
    commands{del_edge mod_rel;NON.void=y; GOV.polarity="-"}}

}

% ===============================================================================================
% Modification of adjectives, adverbs, nouns, prepositions and verbs by default
package default_modif{

  rule mod_adv{
    pattern{ADV[cat=ADV,lemma <>"ne"]; mod_rel: GOV -[mod]-> ADV}
    commands{del_edge mod_rel; add_edge GOV -[amr-mod]-> ADV}}

  rule mod_cat{
    pattern{MOD[cat=C|N|P|PREF]; mod_rel: GOV -[dep|mod]-> MOD}
    without{MOD -[^ mod.rel]-> GOV}
    commands{del_edge mod_rel; add_edge GOV -[amr-mod]-> MOD}}

  rule dep{
    pattern{dep_rel: GOV -[dep]-> MOD}
    commands{del_edge dep_rel; add_edge GOV -[amr-mod]-> MOD}}

  rule depde{
    pattern{depde_rel: GOV -[dep.de]-> MOD}
    commands{del_edge depde_rel; add_edge GOV -[prep-de]-> MOD}}

}
